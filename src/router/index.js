import Vue from "vue";
import Router from "vue-router";
import { auth } from "@/firebase";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      params: true,
      component: () =>
        import(/* webpackChunkName:"Home" */ "../views/Index.vue"),
      children: [
         {
          path: "/encuesta/:id",
          name: "encuesta",
          params:true,
          component: () =>
            import(
              /* webpackChunkName:"Encuesta" */ "../views/encuesta/Encuesta.vue"
            ),
          children: [
            {
              path: "resultados/:pid",
              name: "resultados",
              params:true,
              component: () =>
                import(
                  /* webpackChunkName:"resultados" */ "../views/encuesta/Resultados.vue"
                ),
              autenticado: true
            },
          ],
        },
        {
          path: "/sesion/login",
          name: "login",
          component: () =>
            import(/* webpackChunkName:"Login" */ "../views/sesion/Login.vue"),
        },
        {
          path: "/crear-encuesta",
          name: "crear-encuesta",
          meta:{
            autenticado: true
          },
          component: () =>
            import(
              /* webpackChunkName:"Crear" */ "../views/encuesta/Crear.vue"
            ),
        },
        {
          path: "/telegram",
          name: "telegram",
          component: () =>
            import(
              /* webpackChunkName:"telegram" */ "../views/telegram/Telegram.vue"
            ),
          meta: {
            autenticado: true,
          },
        },

        {
          path: "/mis-encuestas",
          name: "mis-encuestas",
          component: () =>
            import(
              /* webpackChunkName:"Creados" */ "../views/encuesta/Creados.vue"
            ),
          meta: {
            autenticado: true,
          },
        },
        {
          path: "/404",
          name: "404",
          component: () =>
            import(
              /* webpackChunkName:"Error404" */ "@/components/NotFound.vue"
            ),
        },
      ],
    },
    {
      path: "*",
      component: () =>
        import(/* webpackChunkName:"Error404" */ "@/components/NotFound.vue"),
    },
  ],
});

router.beforeEach((to, from, next) => {
  let user = auth.currentUser;

  if (to.matched.some((record) => record.meta.autenticado)) {
    if (user) {
      next();
    } else {
      next({ name: "login" });
    }
  } else {
    next();
  }
});

export default router;
