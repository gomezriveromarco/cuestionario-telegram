import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import vuetify from './plugins/vuetify';
import Vuelidate from 'vuelidate'
import {auth} from './firebase'
Vue.config.productionTip = false
Vue.use(Vuelidate)

 
auth.onAuthStateChanged(user=>{
 
  if(user){
   store.dispatch('sesion/iniciarSesion',user.uid)
   .then(()=>{
     iniciarVue()
   })
  }else{
   store.dispatch('sesion/cerrarSesion')
   .then(()=>{
    iniciarVue()
  })
  }
})

let vue = null
function iniciarVue(){
  if(!vue){
    new Vue({
      store,
      vuetify,
      router,
      render: h => h(App),
    }).$mount('#app')
 
  }
}
