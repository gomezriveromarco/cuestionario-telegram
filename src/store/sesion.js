import { auth, db } from "@/firebase";
import router from "../router";
  
export default {
  namespaced: true,
  state: {
    usuario: null,
  },
  mutations: {
    actualizarUsuario(state,usuario){
        state.usuario=usuario
      },
  },
  actions: {
    async iniciarSesion({ commit, getters }, uid) {
      try {
        let doc = await db
          .collection("users")
          .doc(uid)
          .get();
        if(doc.exists){
          let usuario = doc.data();
          commit("actualizarUsuario", usuario);
          switch (router.currentRoute.name) {
            case "login":
              router.push({ name: "mis-encuestas" });
              commit("mostrarExito", getters.saludo, { root: true });

              break;
          }
        }
      } catch (error) {
        commit("mostrarError", "Ocurrió un error consultando tu informacion.", {
          root: true,
        });
      }
    },
    cerrarSesion({ commit }) {
      auth.signOut();
      commit("actualizarUsuario", null);
    },
  },
  getters: {
    saludo(state) {
      if (!state.usuario) {
        return "";
      }
      let vocal = state.usuario.sexo && state.usuario.sexo == "F" ? "a" : "o";
      return `Bienvenid${vocal} ${state.usuario.nombres}!!`;
    },
  },
};
