import Vue from 'vue'
import Vuex from 'vuex'
import sesion from './sesion'

Vue.use(Vuex)

export default new Vuex.Store({
  
  state: {
    notificacion:{
      vista:false,
      mensaje:'',
      color:'info'
    },
    ocupado:{
      vista:false,
      titulo:'',
      mensaje:''
    }
   },
   mutations: {
  
  
   mostrarInformacion(state,mensaje){
     state.notificacion.mensaje=mensaje
     state.notificacion.color='info'
     state.notificacion.vista=true
   },
   mostrarExito(state,mensaje){
     state.notificacion.mensaje=mensaje
     state.notificacion.color='info'
     state.notificacion.vista=true
   },
   mostrarAdvertencia(state,mensaje){
     state.notificacion.mensaje=mensaje
     state.notificacion.color='warning'
     state.notificacion.vista=true
   },
   mostrarError(state,mensaje){
     state.notificacion.mensaje=mensaje
     state.notificacion.color='error'
     state.notificacion.vista=true
   },
   ocultarNotificacion(state){
     state.notificacion.vista=false
   },
   mostrarOcupado(state,ocupado){
     state.ocupado.titulo=ocupado.titulo
     state.ocupado.mensaje=ocupado.mensaje
     state.ocupado.vista=true
   },
   ocultarOcupado(state){
     state.ocupado.vista=false
   },
  
 
   },
  actions: {

  },
  getters:{

  },
  modules:{
    sesion
  }
})

