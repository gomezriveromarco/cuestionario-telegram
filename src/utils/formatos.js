const formatear = timestamp =>{
    let a = new Date(timestamp * 1000);
    let months = [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Setiembre",
      "Octubre",
      "Noviembre",
      "Diciembre",
    ];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let fecha = date + " de " + month + " del " + year;
    return fecha;
  }
  
  export { formatear }