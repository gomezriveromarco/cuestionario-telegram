const nombreCompuesto = value => {
    if(value===undefined || value===null || value===''){
        return true
    }
    return /^(?! )(?!\n)[^\n][a-z A-Z -áéíóúÁÉÍÓÚüÜñÑ ",]+$/.test(value.match(/[^\r\n]+/g))

  }
  const titulo = value => {
    if(value===undefined || value===null || value===''){
        return true
    }
    return /^(?! )(?!\n)[^\n][a-z 0-9 A-Z -áéíóúÁÉÍÓÚüÜñÑ ",]+$/.test(value.match(/[^\r\n]+/g))

  }
  
  export { nombreCompuesto ,titulo}