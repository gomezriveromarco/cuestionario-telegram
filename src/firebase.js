import firebase from 'firebase/app'
import "firebase/database";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
 
  const config = {
    apiKey: "AIzaSyBkfXtJoBHWCVpz7Kv_3yn4e7r1mVbqV2E",
    authDomain: "asistente-difods.firebaseapp.com",
    databaseURL: "https://asistente-difods.firebaseio.com",
    projectId: "asistente-difods",
    storageBucket: "asistente-difods.appspot.com",
    messagingSenderId: "661857068022",
    appId: "1:661857068022:web:5a346c1b8f8da0b0265dd7",
    measurementId: "G-S9F45HVLD2"
  };

  firebase.initializeApp(config);
  const auth=firebase.auth()
  const db=firebase.firestore()
  const storage=firebase.storage()
 
  export{
      firebase,
      auth,
      db,
      storage
  }